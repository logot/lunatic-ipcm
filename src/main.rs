use std::io::{self, Read, Write};

use lunatic::{
    net::{TcpListener, TcpStream},
    spawn, spawn_link, Mailbox,
};

// you can rename this function to main to run it locally
fn main() -> io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:7878").expect("should be able to bind");

    while let Ok((stream, _)) = listener.accept() {
        spawn!(|stream| read_stream(stream).expect("should have handled request"));
    }
    Ok(())
}

fn read_stream(mut stream: TcpStream) -> io::Result<()> {
    // create a handler process and link to it so that if the handler crashed we also stop reading
    // from the stream because we want to fail early here. Keep in mind, that this is not what you
    // always want though, so pick the approach that better fits you.
    let stream_clone = stream.clone();
    let handler_process = spawn_link!(|stream_clone, mailbox: Mailbox<Vec<u8>>| handle_request(
        stream_clone,
        mailbox
    )
    .expect("should have handled request"));
    // Read from stream in a loop
    loop {
        let mut request = vec![0u8; 1024];
        stream.read_exact(&mut request)?;
        // Send Vec<u8> to handler process
        handler_process.send(request);
    }
}

fn handle_request(mut stream: TcpStream, mailbox: Mailbox<Vec<u8>>) -> io::Result<()> {
    let mut response: Vec<u8> = format!(
        "HTTP/1.1 200 OK\r\n
    Content-Length: 0\r\n
    \r\n"
    )
    .into_bytes();

    // Listen for incoming parsed requests
    // NOTE: this will block the function until something arrives in the mailbox. This will not
    // however affect any other running processes

    let request = mailbox.receive();
    // 2. Print the contents of request
    println!("Request: {:?}", String::from_utf8(request));
    // 3. write back a 200 OK response
    stream.write_all(&response)?;
    Ok(())
}
