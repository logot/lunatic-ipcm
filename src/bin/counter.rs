use lunatic::{
    abstract_process,
    ap::{Config, DeferredResponse},
    AbstractProcess, Tag,
};
use serde::{Deserialize, Serialize};

// Process state
#[derive(Debug, Serialize, Deserialize)]
struct Counter(u32);

#[abstract_process]
impl Counter {
    // function for initialising the state
    // NOTE: the start arg type must implement Serialize
    #[init]
    fn init(_: Config<Self>, start: u32) -> Result<Self, ()> {
        Ok(Self(start))
    }

    #[terminate]
    fn terminate(self) {
        println!("Shutdown process");
    }

    #[handle_link_death]
    fn handle_link_death(&self, _tag: Tag) {
        println!("Link trapped");
    }

    // create a function that handles a fire-and-forget message meaning the sender does not expect
    // an response
    #[handle_message]
    fn increment(&mut self) {
        self.0 += 1
    }

    // create a handler for a request to which the sender expects a u32 in this case
    #[handle_request]
    fn count(&self) -> u32 {
        self.0
    }

    // this case is more special and it allows you to save a deferredResponse for later when you
    // have enough data too send the response. We will get into this pattern deeper a bit later
    #[handle_request]
    fn add_to_count(&self, a: u32, b: u32, dr: DeferredResponse<u32, Self>) {
        dr.send_response(self.0 + a + b)
    }

    // a function annotated with #[handle_request] will handle an incoming message AND send a
    // response. In this case the response type is a u32.
    // NOTE: the argument and return types must impelement Serialize + Deserialize
    //
    // these calls are handled sequentially by the process in the order of arrival
    #[handle_request]
    fn increment_and_report(&mut self) -> u32 {
        self.0 += 1;
        self.0
    }

    // a function annotated with #[handle_deferred_request] will handle an incoming message And
    // send a response whenever it chooses. In this case the response type is a u32.
    // NOTE: the argument and return types must impelement Serialize + Deserialize
    //
    // these calls are also executed in order of message arrival but this here function can choose
    // to postpone responding for a later point in time, perhaps when another message arrives that
    // will mark deferred request completed. More details below
    #[handle_deferred_request]
    fn increment_and_report_whenever_you_feel_like_it(&mut self, dr: DeferredResponse<u32, Self>) {
        self.0 += 1;
        dr.send_response(self.0) // this would just send the response immediately
    }
}

// fn process_a(_: Mailbox<()>) {
fn main() {
    // create a new counter that starts from 0
    let counter = Counter::start(0).expect("should spawn counter process");
    // this will increment the counter three times, AND will wait for completion of the counter
    // process and only then the next instruction in this function will be executed
    counter.increment_and_report();

    // e.g. this runs only if the line above has completed
    println!("Result of incrementing {}", counter.increment_and_report());
    println!("Result of incrementing {}", counter.increment_and_report());
}
